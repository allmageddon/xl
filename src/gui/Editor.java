package gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JTextField;

import model.Sheet;
import model.UpdateType;

public class Editor extends JTextField implements Observer {

	private class KeyListener extends KeyAdapter {

		public void keyPressed(KeyEvent event) {
			if (event.getKeyCode() == KeyEvent.VK_ENTER)
				sheet.evalSlot(currentSlot.getCurrentSlot(), Editor.this.getText());
		}
	}

	private Sheet sheet;
	private CurrentSlotState currentSlot;

	public Editor(Sheet sheet, CurrentSlotState currentSlot) {
		setBackground(Color.WHITE);
		this.sheet = sheet;
		this.currentSlot=currentSlot;

		sheet.addObserver(this);
		currentSlot.addObserver(this);
		addKeyListener(new KeyListener());

	}

	@Override
	public void update(Observable arg0, Object arg1) {
		if ((UpdateType) arg1 == UpdateType.CurrentSlotChange
				|| (UpdateType) arg1 == UpdateType.SheetUpdate) {
			setText(sheet.getEditorString(currentSlot.getCurrentSlot()));
		}

	}

}