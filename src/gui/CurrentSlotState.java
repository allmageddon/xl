package gui;

import java.util.Observable;

import model.UpdateType;

public class CurrentSlotState extends Observable {
	
	private String currentSlot;

	public CurrentSlotState(){
		this.setCurrentSlot("A1");
	}

	public String getCurrentSlot() {
		return currentSlot;
	}

	public void setCurrentSlot(String currentSlot) {
		this.currentSlot = currentSlot;
		setChanged();
		notifyObservers(UpdateType.CurrentSlotChange);
	}
	
	

}
