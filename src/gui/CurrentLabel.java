package gui;

import java.awt.Color;
import java.util.Observable;
import java.util.Observer;

import model.Sheet;
import model.UpdateType;

public class CurrentLabel extends ColoredLabel implements Observer {
	CurrentSlotState current;
    public CurrentLabel(CurrentSlotState current) {
        super("A1", Color.WHITE);
        current.addObserver(this);
        this.current=current;
    }

	@Override
	public void update(Observable o, Object arg) {
		if((UpdateType) arg == UpdateType.CurrentSlotChange){
			setText(current.getCurrentSlot());
			
		}
		
	}
}