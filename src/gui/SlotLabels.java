package gui;

import java.awt.Color;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import javax.swing.SwingConstants;

import model.Sheet;
import model.UpdateType;

public class SlotLabels extends GridPanel implements Observer {
    private Map<String, SlotLabel> labels=new HashMap<String,SlotLabel>();
	private Sheet sheet;
	private SlotLabel currentLabel;
	private CurrentSlotState current;
    public SlotLabels(int rows, int cols, Sheet sheet, CurrentSlotState current) {
        super(rows + 1, cols);
        sheet.addObserver(this);
        current.addObserver(this);
    	this.sheet = sheet;
    	this.current=current;
        for (char ch = 'A'; ch < 'A' + cols; ch++) {
            add(new ColoredLabel(Character.toString(ch), Color.LIGHT_GRAY,
                    SwingConstants.CENTER));
        }
        for (int row = 1; row <= rows; row++) {
            for (char ch = 'A'; ch < 'A' + cols; ch++) {
            	String addr = ""+ch+row;
                SlotLabel label = new SlotLabel(addr , current);
                add(label);
                labels.put(addr, label);
            }
        }
       
        currentLabel = labels.get(current.getCurrentSlot());

        
        
        currentLabel.setBackground(Color.YELLOW);
    }
    
    private void refresh(){
		Map<String, String> newLabels = sheet.getCellStrings();
			
		for (String address : labels.keySet()) {
			if(newLabels.containsKey(address)){
				labels.get(address).setText(newLabels.get(address));
				
			}else{
				labels.get(address).setText("");
			}
			
		}
    }

	@Override
	public void update(Observable arg0, Object arg1) {
		if ((UpdateType)arg1 == UpdateType.SheetUpdate) {
			refresh();
		}
		else if ((UpdateType)arg1 == UpdateType.CurrentSlotChange) {
			currentLabel.setBackground(Color.WHITE);
			currentLabel = labels.get(current.getCurrentSlot());
			currentLabel.setBackground(Color.YELLOW);
		}else if((UpdateType)arg1 == UpdateType.Error){
			
			printErrors();
		}
		
	}

	private void printErrors() {
		Map<String, String> newLabels = sheet.getEditorStrings();
		
		for (String address : labels.keySet()) {
			if(newLabels.containsKey(address)){
				labels.get(address).setText(newLabels.get(address));
				
			}else{
				labels.get(address).setText("");
			}
			
		}
		
	}
}
