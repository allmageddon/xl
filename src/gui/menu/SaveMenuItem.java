package gui.menu;

import gui.StatusLabel;
import gui.XL;
import java.io.FileNotFoundException;
import javax.swing.JFileChooser;

import model.Sheet;

class SaveMenuItem extends OpenMenuItem {
    public SaveMenuItem(XL xl, StatusLabel statusLabel , Sheet sheet) {
        super(xl, statusLabel, "Save", sheet);
    }

    protected void action(String path) throws FileNotFoundException {
    	sheet.printToFile(path);
    }

    protected int openDialog(JFileChooser fileChooser) {
        return fileChooser.showSaveDialog(xl);
    }
}