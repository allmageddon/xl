package gui.menu;

import gui.CurrentSlotState;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenuItem;

import model.Sheet;

class ClearMenuItem extends JMenuItem implements ActionListener {
    private Sheet sheet;
	private CurrentSlotState current;
    

	public ClearMenuItem( Sheet sheet, CurrentSlotState current) {
        super("Clear");
        addActionListener(this);
        this.sheet=sheet;
        this.current=current;
    }

    public void actionPerformed(ActionEvent e) {
        sheet.clear(current.getCurrentSlot());
    }
}