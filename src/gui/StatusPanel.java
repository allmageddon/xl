package gui;

import static java.awt.BorderLayout.CENTER;
import static java.awt.BorderLayout.WEST;
import model.Sheet;

public class StatusPanel extends BorderPanel  {
    protected StatusPanel(StatusLabel statusLabel, CurrentSlotState current) {
        add(WEST, new CurrentLabel(current));
        add(CENTER, statusLabel);
    }
}