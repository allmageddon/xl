package gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import model.Sheet;

public class SlotLabel extends ColoredLabel implements ActionListener {
	Sheet model;
	String address;
	private CurrentSlotState current;

	private class ClickListener extends MouseAdapter {

		public void mouseClicked(MouseEvent event) {
			current.setCurrentSlot(address);
			
			
		}
	}

	public SlotLabel( String address, CurrentSlotState current) {
		super("                    ", Color.WHITE, RIGHT);
		this.current =current;
		this.address = address;

		this.addMouseListener(new ClickListener());
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub

	}
}