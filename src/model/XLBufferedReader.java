package model;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Map;


public class XLBufferedReader extends BufferedReader {
    public XLBufferedReader(String name) throws FileNotFoundException {
        super(new FileReader(name));
    }

    public void load(SlotFactory factory, SlotMap map) {
        try {
            while (ready()) {
                String string = readLine();
                int i = string.indexOf('=');
                String address = string.substring(0, i);
                String editorString = string.substring(i+1);
                map.put(factory.build(address, editorString));
            }
        } catch (Exception e) {
            throw new XLException(e.getMessage());
        }
    }
}
