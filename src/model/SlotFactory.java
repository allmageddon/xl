package model;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import expr.ExprParser;

public class SlotFactory {
	ExprParser parser = new ExprParser();

	public Slot build(String address, String s) {
		Pattern empty = Pattern.compile("\\s*");
		Pattern comment = Pattern.compile("\\s*#.*");
		if (empty.matcher(s).matches()) {
			return new EmptySlot(address);
		} else if (comment.matcher(s).matches()) {
			return new CommentSlot(address, s);
		} else {
			try {
				return new ExprSlot(address, s, parser.build(s));
			} catch (IOException e) {
				throw new XLParserException("Invalid Expression in slot "+address+".");
			} catch (XLException e) {
				throw new XLParserException("Invalid Expression in slot "+address+".");
			}
		}
	}

}
