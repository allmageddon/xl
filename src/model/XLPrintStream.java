package model;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Map.Entry;

//TODO move to another package
public class XLPrintStream extends PrintStream {
    public XLPrintStream(String fileName) throws FileNotFoundException {
        super(fileName+".xl");
    }

    // TODO Change Object to something appropriate
    public void save(SlotMap map) {
        for (Entry<String, Slot> entry : map) {
            print(entry.getKey());
            print('=');
            println(entry.getValue().getEditorString());
        }
        flush();
        close();
    }
}
