package model;

import expr.Environment;
import expr.Expr;

public class ExprSlot extends Slot {
	private Expr expr;
	
	public ExprSlot(String address, String editorString, Expr expr){
		super(address, editorString);
		this.expr=expr;
		
	}

	@Override
	public double value(Environment e) {
		return expr.value(e);
	}


	@Override
	public String cellString(Environment e) {
		return ""+e.value(address);

	}
}
