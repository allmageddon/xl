package model;

import expr.Environment;

public abstract class Slot {
	protected String address;
	
	protected String editorString;
	
	public Slot(String address, String editorString) {
		this.editorString = editorString;
		this.address=address;
	}
	public String getEditorString(){
		return editorString;
	}	
	public String getAddress(){
		return address;
	}
	public abstract double value(Environment e);
	public abstract String cellString(Environment e);
	
	
	
}
