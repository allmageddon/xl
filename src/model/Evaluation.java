package model;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;


import expr.Environment;

public class Evaluation implements Environment {
	private SlotMap map;
	private Map<String, Double> cache;
	private Set<String> usedVariables;
	
	public Evaluation(SlotMap map){
		this.map = map;
		cache = new HashMap<String, Double>();
		usedVariables = new HashSet<String>();
		calculate();
	}

	@Override
	public double value(String name) {
		if (cache.containsKey(name)) return cache.get(name);
		if (usedVariables.contains(name)) throw new XLException("Circular variable reference involving slot "+name+".");
		usedVariables.add(name);
		double val = map.get(name).value(this);
		usedVariables.remove(name);
		cache.put(name, val);
		return val;
	}
	private void calculate(){
		for (Entry<String, Slot> e : map) {
			value(e.getKey());
		}
	}
	


}
