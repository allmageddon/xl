package model;

import expr.Environment;

public class EmptySlot extends Slot {

	public EmptySlot(String address) {
		super(address,"");
	}


	@Override
	public double value(Environment e) {
		throw new XLException("Invalid Reference: Slot "+address+" is empty.");
	}


	@Override
	public String cellString(Environment e) {
		throw new UnsupportedOperationException();
	}

}
