package model;

public class XLParserException extends XLException {
    public XLParserException(String message) {
        super(message);
    }
}
