package model;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class SlotMap implements Iterable<Entry<String, Slot>> {

	private Map<String, Slot> map = new HashMap<String, Slot>();

	public Slot get(String addr) {
		if (!map.containsKey(addr))
			return new EmptySlot(addr);
		return map.get(addr);
	}

	public void put(Slot slot) {
		if (slot.getClass() == EmptySlot.class) {
			map.remove(slot.getAddress());
		} else {
			map.put(slot.getAddress(), slot);
		}
	}

	public Iterator<Entry<String, Slot>> iterator() {
		return map.entrySet().iterator();

	}
}
