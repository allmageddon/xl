package model;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Observable;

public class Sheet extends Observable {
	private SlotMap slots = new SlotMap();
	private Evaluation currentEvaluation = new Evaluation(slots);
	private SlotFactory factory = new SlotFactory();
	private String errorString;



	public void clear(String address) {
		evalSlot(address, "");
	}

	public void clearAll() {
		slots = new SlotMap();
		eval();
	}

	public Map<String, String> getCellStrings() {
		Map<String, String> results = new HashMap<String, String>();
		for (Entry<String, Slot> e : slots) {
			results.put(e.getKey(), e.getValue().cellString(currentEvaluation));
		}
		return results;
	}
	
	public Map<String, String> getEditorStrings() {
		Map<String, String> results = new HashMap<String, String>();
		for (Entry<String, Slot> e : slots) {
			results.put(e.getKey(), e.getValue().getEditorString());
		}
		return results;
	}
	
	private void eval() {
		try {
			currentEvaluation = new Evaluation(slots);
			setChanged();
			notifyObservers(UpdateType.SheetUpdate);
		} catch (XLException e) {
			sendError(e.getMessage());
		}
	}

	public String getEditorString(String address) {
		return slots.get(address).getEditorString();
	}



	public void evalSlot(String address, String text) {
		try {
			slots.put(factory.build(address, text));
			eval();
		} catch (XLException e) {
			sendError(e.getMessage());
			setChanged();
			notifyObservers(UpdateType.Error);
		}
		
	}

	public String getErrorString() {
		return errorString;
	}
	
	private void sendError(String errorString) {
		this.errorString = errorString;
		setChanged();
		notifyObservers(UpdateType.Error);
	}
	
	public void printToFile(String path){
		try {
			XLPrintStream printer= new XLPrintStream(path);
			printer.save(slots);
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			sendError(e.getMessage());
		}
		
	}

	public void loadFromFile(String path) {
		try {
			clearAll();
			XLBufferedReader reader= new XLBufferedReader(path);
			slots=new SlotMap();
			reader.load(factory, slots);
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			sendError(e.getMessage());
		}
		eval();
		
		
	}

}
