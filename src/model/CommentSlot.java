package model;

import expr.Environment;

public class CommentSlot extends Slot {

	
	public CommentSlot(String address, String editorString) {
		super(address, editorString);
	}

	@Override
	public double value(Environment e) {
		return 0;
	}


	@Override
	public String cellString(Environment e) {
		return editorString.substring(1);
	}


}
